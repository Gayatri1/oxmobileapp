angular.module('app.controllers', []).constant('$globalUrl', 'http://www.oxcfe.be/api/')


.controller('homeCtrl', ['$scope', '$stateParams', '$http', '$ionicLoading', '$timeout', '$ionicHistory', '$state', '$ionicPlatform', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicLoading, $timeout, $ionicHistory, $state, $ionicPlatform, $globalUrl)
{

	
	$ionicLoading.show();
	// Homepage Slider 
	$http.get($globalUrl + "homePageSlider")
		.then(function (response)
		{
			$scope.banner = response.data.data;
			$ionicLoading.hide();
		});
	
	$scope.currentPage = 0;
	$scope.pageSize = 2;	 
	
	$scope.ITEMS = [];

	$http.get($globalUrl + "dayofferproducts")
	.then(function (response)
	{
		$scope.homepageItems = response.data;

		angular.forEach($scope.homepageItems.data, function (value, key)
		{
			$scope.ITEMS.push({ "ID": value.Id, "IMAGE": value.DefaultPictureModel.FullSizeImageUrl, "NAME": value.Name, "PRICE": value.ProductPrice.Price, "QTY": value.Quantity, "DESC": value.FullDescription });

		})	 
	
	});


	$scope.numberOfPages = function ()
	{
		return Math.ceil($scope.ITEMS.length / $scope.pageSize);
	}

	
	// Lunch  Offer	
	$scope.currentPage2 = 0;
	$scope.pageSize2 = 2;
	$scope.LunchITEMS = [];
	$http.get($globalUrl + "lunchproducts")
	.then(function (response)
	{
		$scope.homepageItems = response.data;
		angular.forEach($scope.homepageItems.data, function (value, key)
		{
			$scope.LunchITEMS.push({ "ID": value.Id, "IMAGE": value.DefaultPictureModel.FullSizeImageUrl, "NAME": value.Name, "PRICE": value.ProductPrice.Price, "QTY": value.Quantity, "DESC": value.FullDescription });
		})
	
	});
	$scope.numberOfPages2 = function ()
	{
		return Math.ceil($scope.LunchITEMS.length / $scope.pageSize2);
	}
	$ionicLoading.hide();
	
	//End

	// DINNER  Offer

	
	$scope.currentPage1 = 0;
	$scope.pageSize1 = 2;

	$scope.DINNERITEMS = [];
	$http.get($globalUrl + "drinkproducts")
	.then(function (response)
	{
		$scope.homepageItems = response.data;
		angular.forEach($scope.homepageItems.data, function (value, key)
		{
			$scope.DINNERITEMS.push({ "ID": value.Id, "IMAGE": value.DefaultPictureModel.FullSizeImageUrl, "NAME": value.Name, "PRICE": value.ProductPrice.Price, "QTY": value.Quantity, "DESC": value.FullDescription });
		})
		
	});
	$scope.numberOfPages1 = function ()
	{
		return Math.ceil($scope.DINNERITEMS.length / $scope.pageSize1);
	}	 
	
	//End.
  
	//Get Product Id using Method
	$scope.showProductDetail = function (id)
	{
		//sessionStorage.setItem('product_detail_id', id);
		window.location.href = "#/side-menu21/page3?productId=" + id;


	}

	//Login user and Guest User code
	 
		$scope.userid = sessionStorage.getItem('loggedin_id');

		$scope.ind = sessionStorage.getItem('all_Id');
		if ($scope.userid == null || $scope.userid == 'undefined' || $scope.userid == undefined)
		{
			$scope.userid = $scope.ind;
		}
	
		if ($scope.userid == null || $scope.userid == 'undefined' || $scope.userid == undefined)
		{ 			
				$http.get($globalUrl + "CreateGuest").then(function (response)
				{
					$scope.guestdata = response.data.data;
					$scope.infoId = $scope.guestdata.Id;
					sessionStorage.setItem('all_Id', $scope.infoId);
					localStorage.setItem("all_Id", $scope.infoId);
				});			
		}
		else
		{				
			$scope.infoId = $scope.userid;
			$scope.ind = $scope.userid;
		}

		var x = '';
		setTimeout(function ()
		{
			x = $scope.infoId;
			sessionStorage.setItem('all_Id', $scope.infoId);
			localStorage.setItem("all_Id", $scope.infoId);
			$scope.ind = sessionStorage.getItem('all_Id');
		}, 900);


	
	//Home Products Adding to Cart
	$scope.addToCart = function (id, Qty)
	{
	
		var enterQty = Qty;	 	
		$scope.ind = sessionStorage.getItem('all_Id');
	
		$scope.productId = id;
		if (enterQty == 1 || enterQty > 0)
		{
			var json = ({ EnteredQuantity: enterQty, ShoppingCartType: 1, CustomerId: $scope.ind, productId: $scope.productId, ProductAttributeValues: "" });
			var mystr = JSON.stringify(json);
		
			str = $globalUrl + "addtoocart?details=" + mystr;
			$http.get(str).success(function (response)
			{
				
				if (response.success == false)
				{
					$('#lblAlertMessage').html(response.message);
					$('.alert').show();
					$('.alertBG').show();
				}
				else
				{
					window.location.href = "#/side-menu21/page7";
					location.reload();
				}

			
			})
		}
		else
		{
			$('#lblAlertMessage').html('Voeg een hoeveelheid toe');
			$('.alert').show();
			$('.alertBG').show();
		}
	}
	//End



}])

.filter('startFrom', function ()
{
	return function (input, start)
	{
		if (input)
		{
			start = +start; //parse to int
			return input.slice(start);
		}
	}
})


.controller('productCtrl', ['$scope', '$stateParams', '$http', '$ionicLoading', '$timeout', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicLoading, $timeout, $state, $globalUrl)
{

	$scope.$on('$ionicView.enter', function ()
	{
		$ionicLoading.show();
		$scope.ITEMS = [];
		$http.get($globalUrl + "category/17")
		.then(function (response)
		{
			$scope.productItems = response.data;
			angular.forEach($scope.productItems, function (value, key)
			{ 			
				$scope.ITEMS.push({ "ID": value.Id, "IMAGE": value.ImageUrl, "NAME": value.Name, "PRICE": value.Price, "STOCKS": value.StockAvailability });
			})
			$ionicLoading.hide();
		});

	});

	//Get Product Id uding Method
	$scope.showProductDetail = function (id)
	{	
		window.location.href = "#/side-menu21/page3?productId=" + id;
	}

	// Add to cart Button on product details page 
	$scope.addToCart = function (Pid)
	{
		var enterQty = $('input#qty').val(); 	
		$scope.profile_info_id = $scope.ind = sessionStorage.getItem('all_Id');	  	
		$scope.productId = Pid;

		if (enterQty == 1 || enterQty > 0)
		{
			var json = ({ EnteredQuantity: enterQty, ShoppingCartType: 1, CustomerId: $scope.profile_info_id, productId: $scope.productId, ProductAttributeValues: "" });
			var mystr = JSON.stringify(json);
			$ionicLoading.show();

			str = $globalUrl + "addtoocart?details=" + mystr;
			$http.get(str)
			.success(function (response)
			{
				if (response.success == false)
				{ 					
					$('#lblAlertMessage').html(response.message);
					$('.alert').show();
					$('.alertBG').show();
				}
				else
				{
					window.location.href = "#/side-menu21/page7";
				}

				$ionicLoading.hide();
			})
		}
		else
		{				
			$('#lblAlertMessage').html('Voeg een hoeveelheid toe');
			$('.alert').show();
			$('.alertBG').show();

		}

	}

}])

.controller('productDetailCtrl', ['$location', '$scope', '$stateParams', '$http', '$ionicLoading', '$timeout', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $ionicLoading, $timeout, $state, $globalUrl)
{

	$scope.id = ($location.search()).productId;	 
	$scope.ind = sessionStorage.getItem('all_Id');
	$scope.thumbImages = [];
	$http.get($globalUrl + "ProductsDetails/" + $scope.id + "/" + $scope.ind)
 .then(function (response)
 {

 	$scope.prodetail = response.data;

 	angular.forEach($scope.prodetail.data.PictureModels, function (value, key)
 	{
 		$scope.thumbImages.push({ "THUMBIMAGE": value.ThumbImageUrl, "FULLIMAGE": value.FullSizeImageUrl, "ImgId": value.Id });

 	})

 	//Add In session 
 	sessionStorage.setItem('enteredQuantity', $scope.prodetail.data.AddToCart.EnteredQuantity);
 	sessionStorage.setItem('shoppingCartType', 1);
 	sessionStorage.setItem('productId', $scope.prodetail.data.AddToCart.ProductId);
 	//End


 });
	//END

	$scope.$on('$ionicView.enter', function ()
	{

		$ionicLoading.show();
		var total = { "customerId": $scope.ind }
		var mystr = JSON.stringify(total);
		$http.get($globalUrl + "OrderSummary/?details=" + mystr)
			 .then(function (response)
			 {
			 	$scope.totalcartPrice = response.data;
			 	$ionicLoading.hide();
			 });

	});

	// Add to cart Button on product details page 
	$scope.addToCart = function (Id)
	{
		var enterQty = $('input#qty').val();
		sessionStorage.setItem('EnteredQty', enterQty)
		$scope.profile_info_id = $scope.ind = sessionStorage.getItem('all_Id');
		$scope.productId = Id;

		if (enterQty == 1 || enterQty > 0)
		{

			var json = ({ EnteredQuantity: enterQty, ShoppingCartType: 1, CustomerId: $scope.profile_info_id, productId: $scope.productId, ProductAttributeValues: "" });
			var mystr = JSON.stringify(json);
			$ionicLoading.show();

			str = $globalUrl + "addtoocart?details=" + mystr;
			$http.get(str)

			.success(function (response)
			{
				if (response.success == false)
				{
					$('#lblAlertMessage').html(response.message);
					$('.alert').show();
					$('.alertBG').show();
				}
				else
				{
					window.location.href = "#/side-menu21/page7";
				}

				$ionicLoading.hide();
			})
		}

		else
		{
			$('#lblAlertMessage').html('Voeg een hoeveelheid toe');
			$('.alert').show();
			$('.alertBG').show();

		}


	}



	//Related products

	$scope.$on('$ionicView.enter', function ()
	{
		$ionicLoading.show();

		$scope.relatedProducts = []
		
		$http.get($globalUrl + "GetRelatedProducts/" + $scope.id)
	 .then(function (response)
	 {
	 	$scope.related = response.data.data;

	 	angular.forEach($scope.related, function (value, key)
	 	{

	 		$scope.relatedProducts.push({ "NAME": value.Name, "IMAGE": value.DefaultPictureModel.ImageUrl, "ID": value.Id, "PRICE": value.ProductPrice.Price });

	 	});
	 	$ionicLoading.hide();
	 });
	});



	//End

	//PRODUCT DETAIL PAGE OPEN

	$scope.relatedProductDetail = function (id)
	{	
		window.location.href = "#/side-menu21/page3?productId=" + id;	 
	}

	//END


	$scope.openReviews = function (id)
	{
		window.location.href = "#/side-menu21/page31?productId=" + id;		
	}

	$scope.EmailFriend = function (pid)
	{
		window.location.href = "#/side-menu21/page29?productId=" + pid; 	
	}



}])

.controller('menuCtrl', ['$scope', '$stateParams', '$http', '$state', '$ionicHistory', '$ionicLoading', '$timeout', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $state, $ionicHistory, $ionicLoading, $timeout, $globalUrl)
{

	$scope.checksign = sessionStorage.getItem('name_user');
	
	if ($scope.checksign == null)
	{
		$scope.showloginlink = true;
		$scope.showprofilelink = false;
	}
	else
	{
		$scope.showloginlink = false;
		$scope.showprofilelink = true;

	}
	
	$('#menu-list-item1').click(function ()
	{
		$('#DvDAGAANBIEDINGEN').show();
		$('#DvLunch').show();
		$('#DvDranken').show();		
		$('.categoryLink').find('a').each(function() {
		$(this).removeClass("active");
		});
	});+

	
	$('#lnkHome').click(function ()
	{
		$('#DvDAGAANBIEDINGEN').show();
		$('#DvLunch').show();
		$('#DvDranken').show();
		$('.categoryLink').find('a').each(function() {
		$(this).removeClass("active");
		});
	});
	
	
	//LOGOUT
	$scope.logout = function ()
	{

		$ionicLoading.show();
		sessionStorage.removeItem('loggedin_id');
		sessionStorage.removeItem('name_user');
		sessionStorage.removeItem('News_Id');
		sessionStorage.removeItem('Blog_Id');
		sessionStorage.removeItem("getCurrentPageUrl");

		sessionStorage.removeItem('all_Id');
		sessionStorage.removeItem('attributeVal');
		sessionStorage.removeItem('attrchkAttOptionId');
		sessionStorage.removeItem('attrchkAttOptionValue');
		//$window.localStorage.clear();
		localStorage.removeItem("all_Id");
		$ionicHistory.clearCache();
		$ionicHistory.clearHistory();

		window.location.href = "#/side-menu21/page1";
		location.reload();
		$state.reload();
		$ionicLoading.hide();

	};

	//Header Logo Link 
	$scope.homeLogo = function ()
	{
		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		$state.go('menu.home');
	}
	//End


	//LOGOUT
	$scope.GotoCart = function ()
	{
		window.location.href = "#/side-menu21/page7";
		location.reload();
	};

	//Header Logo Link 
	$scope.homeLogo = function ()
	{
		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		$state.go('menu.home');
	}
	//End

}])

.controller('loginCtrl', ['$scope', '$stateParams', '$http', '$ionicPopup', '$ionicHistory', '$timeout', '$state', '$ionicLoading', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup, $ionicHistory, $timeout, $state, $ionicLoading, $globalUrl)
{

	$scope.user = {};

	var rem_UserId = localStorage.getItem("_userId");
	var rem_UserPwd = localStorage.getItem("_userPassword");
	if (rem_UserId != '' || rem_UserId != 'undefined' || rem_UserId != undefined)
	{
		$scope.user.email = rem_UserId;
	}
	if (rem_UserPwd != '' || rem_UserPwd != 'undefined' || rem_UserPwd != undefined)
	{
		$scope.user.password = rem_UserPwd;
	}



	$scope.ind = sessionStorage.getItem('all_Id');


	if ($scope.ind == null || $scope.ind == "undefined" || $scope.ind == undefined)
	{			
		$scope.ind = 0;  		
	}
	$scope.showRegister = true;
	$scope.regUser = sessionStorage.getItem('loggedin_id');

	if ($scope.regUser != null)
	{
		$scope.showRegister = false;
	}


	$scope.login = function ()
	{

		var json = { "UserId": $scope.user.email, "Password": $scope.user.password, "GuestID": $scope.ind };
		var mystr = JSON.stringify(json);

		if ($scope.user.email == null || $scope.user.password == null)
		{
			$('#lblAlertMessage').html("Gelieve een gebruikersnaam en wachtwoord in te voeren.");
			$('.alert').show();
			$('.alertBG').show();
		}
		else
		{

			$ionicLoading.show();

			$http.get($globalUrl + "apilogin?json=" + mystr).success(function (response)
			{

				$scope.user_details = response;

				if ($scope.user_details.success == true)
				{

					sessionStorage.setItem('role', $scope.user_details.data.Customerroles);
					sessionStorage.setItem('name_user', $scope.user_details.data.FirstName);
					sessionStorage.setItem('loggedin_id', $scope.user_details.data.CustomerId);
					sessionStorage.setItem('all_Id', $scope.user_details.data.CustomerId);
					localStorage.setItem("all_Id", $scope.user_details.data.CustomerId);
					$scope.ind = sessionStorage.getItem('all_Id');

			
					var getCurrentPageUrl = sessionStorage.getItem("getCurrentPageUrl");
					if (getCurrentPageUrl != null)
					{

						$scope.Attval = sessionStorage.getItem("attributeVal");

						//TextBox Id
						$scope.cChkAttOption = sessionStorage.getItem("attrchkAttOptionId");
						//TextBox value
						$scope.cChkAttOptionValue = sessionStorage.getItem("attrchkAttOptionValue");

						if ($scope.Attval == undefined || $scope.Attval == 'undefined')
						{
							$scope.selectedOption = 2;
							$scope.selectedOptionValue = 4;
							window.location.href = "#/side-menu21/page32?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.cChkAttOption + "&cChkAttOptionValue=" + $scope.cChkAttOptionValue;;
						}
						else if ($scope.Attval == 4)
						{
							$scope.selectedOption = 2;
							$scope.selectedOptionValue = 4;
							window.location.href = "#/side-menu21/page32?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.cChkAttOption + "&cChkAttOptionValue=" + $scope.cChkAttOptionValue;;
						}
						else
						{
							$scope.selectedOption = 2;
							$scope.selectedOptionValue = 3;
							window.location.href = "#/side-menu21/page33?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.cChkAttOption + "&cChkAttOptionValue=" + $scope.cChkAttOptionValue;;
						}

						location.reload();
						
					}
					else
					{
						$ionicHistory.nextViewOptions({
							disableBack: true
						});

						window.location.href = "#/side-menu21/page1";
						location.reload();
						$state.go('menu.home');
					}

					//backview();

				}
				else
				{
					$('#lblAlertMessage').html("Uw inloggegevens kloppen niet, probeer opnieuw.");
					$('.alert').show();
					$('.alertBG').show();
				}
				// remove the instance of login page, when user moves to profile page.
				// if you dont use it, you can get to login page, even if you are already logged in .
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true
				});


			})
			//.error(function ()
			 .error(function (error, status)
			 {  						
			 	$('#lblAlertMessage').html("Uw inloggegevens kloppen niet, probeer opnieuw.");
			 	$('.alert').show();
			 	$('.alertBG').show();
			 });

			$ionicLoading.hide();

		}

	};

	// Goto Home page
	$scope.homeLogo = function ()
	{
		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		$state.go('menu.home');
	}
	//End


	// popup for email enter on forgot password click link
	// When button is clicked, the popup will be shown...
	$scope.showPopup = function ()
	{
		$scope.data = {}

		// Custom popup
		var myPopup = $ionicPopup.show({
			template: '<input type = "text" ng-model = "data.model">',
			title: 'Enter Your Email',	 		
			scope: $scope,

			buttons: [
						{ text: 'Cancel' }, {
							text: '<b>Send</b>',
							type: 'button-positive',
							onTap: function (e)
							{ 								
								var json_new = { Email: $scope.data.model }
								var mystr_new = JSON.stringify(json_new);

								$http.get($globalUrl + "forgot?details=" + mystr_new)
					  .then(function (response)
					  {
					  	$scope.email_sent = response.data;						  
					  	$('#lblAlertMessage').html($scope.email_sent.message);
					  	$('.alert').show();
					  	$('.alertBG').show();
					  });
								if (!$scope.data.model)
								{
									//don't allow the user to close unless he enters model...
									e.preventDefault();
								} else
								{
									return $scope.data.model;
								}
							}
						}
			]
		});

		myPopup.then(function (res)
		{
			console.log('Tapped!', res);
		});
	};


	$scope.check = 0;
	$scope.RemeberCred = function (data)
	{
		if (data)
		{
			//Set Id and password to local storage
			localStorage.setItem("_userId", $('#loginEmail').val().trim());
			localStorage.setItem("_userPassword", $('#loginPass').val().trim());
		}
	};
	//END

}])

.controller('signupCtrl', ['$scope', '$stateParams', '$http', '$ionicPopup', '$state', '$ionicHistory', '$timeout', '$globalUrl', '$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup, $state, $ionicHistory, $timeout, $globalUrl, $ionicLoading)
{

	$scope.ind = sessionStorage.getItem('all_Id');



	$scope.signup = function (data)
	{
		var json = { Email: data.Email, Gender: 'M', FirstName: data.FirstName, LastName: data.LastName, Id: $scope.ind };

		var mystr = JSON.stringify(json);
		$ionicLoading.show();

		//var link = 'http://www.oxcfe-test.be/api/apisignup?json=' + mystr;

		var link = $globalUrl + "apisignup?json=" + mystr;
		$http.get(link, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
		 .then(function (res)
		 {
		 	$scope.response = res.data;
		 	$ionicLoading.hide();
		 	if ($scope.response.success == true)
		 	{
		 		$('#lblAlertMessage').html('Uw account is aangemaakt, gelieve uw mail te raadplegen voor uw accountinformatie');
		 		$('.alert').show();
		 		$('.alertBG').show();

		 		//no back option
		 		$ionicHistory.nextViewOptions({
		 			disableAnimate: false,
		 			disableBack: false
		 		});

		 		sessionStorage.setItem('loggedin_id', $scope.ind);
		 		window.location.href = "#/side-menu21/page1";
		 		location.reload();

		 	}
		 	else if ($scope.response.success == false)
		 	{
		 		$ionicLoading.hide();
		 		$('#lblAlertMessage').html($scope.response.message);
		 		$('.alert').show();
		 		$('.alertBG').show();
		 	}
		 	else
		 	{
		 		$ionicLoading.hide();
		 		$scope.title = "success";
		 		$scope.template = "Gelieve in te loggen";
		 	}

		 });

	}

}])

.controller('profileCtrl', ['$scope', '$stateParams', '$http', '$ionicPopup', '$state', '$ionicHistory', '$timeout', '$ionicLoading', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup, $state, $ionicHistory, $timeout, $ionicLoading, $globalUrl)
{

	$scope.loggedin_id = sessionStorage.getItem('loggedin_id');	 	
	$scope.ind = sessionStorage.getItem('all_Id');
	$scope.user = sessionStorage.getItem('name_user');
  				
	if ($scope.user != null || $scope.user != undefined)
	{
		$ionicLoading.show();
		$http.get($globalUrl + "GetUserInfo/" + $scope.ind).then(function (response)
		{
			$scope.UserProfile = response.data.data;
			$scope.profilePicUrl = $scope.UserProfile.PersonalPictureModel.ImageUrl;
			if ($scope.profilePicUrl == null)
			{
				$scope.profilePicUrl = $scope.UserProfile.PictureModel.ImageUrl;
			}
			$ionicLoading.hide();

			$http.get($globalUrl + "generatebarcode/" + $scope.UserProfile.PersonalNumber).then(function (responsedata)
			{
				$scope.Barcode = responsedata.data.Img;
			});
		});

	}
	else
	{
		window.location.href = "#/side-menu21/page4";
		$('#lblAlertMessage').html('Gelieve in te loggen');
		$('.alert').show();
		$('.alertBG').show();
	}

	$scope.pwdChangePage = function ()
	{
		window.location.href = "#/side-menu21/page34";
	}

	//}); 
}])


.controller('editProfileCtrl', ['$scope', '$stateParams', '$http', '$ionicPopup', '$state', '$ionicHistory', '$timeout', '$ionicLoading', '$globalUrl',

function ($scope, $http, $ionicPopup, $state, $ionicHistory, $globalUrl)
{
	angular.element(document).ready(function ()
	{
		$scope.ind = sessionStorage.getItem('all_Id'); 	
		$http.get($globalUrl + "customerInformation/" + $scope.ind)
	 .then(function (response)
	 {
	 	$scope.address = response.data;
	 });

	});

	$scope.update = function ()
	{
		$scope.ind = sessionStorage.getItem('all_Id');
		var FirstName = $('#Firstname').val();
		var Lastname = $('#Lastname').val();
		var Address1 = $('#Address1').val();
		var City = $('#City').val();
		var StateProvince = $('#StateProvince').val();
		var Country = $('#Country').val();
		var PhoneNumber = $('#PhoneNumber').val();
		var ZipPostalCode = $('#ZipPostalCode').val();
		var Email = $('#Email').val();
		//	alert(FirstName + Lastname + Address1 + City + StateProvince + Country + PhoneNumber + ZipPostalCode + Email);	
		//	ShowDialogBox('Alert', FirstName + Lastname + Address1 + City + StateProvince + Country + PhoneNumber + ZipPostalCode + Email, 'Ok');	 
	}

}])

.controller('cartCtrl', ['$scope', '$stateParams', '$http', '$state', '$ionicLoading', '$ionicHistory', '$timeout', '$state', '$globalUrl',  // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $state, $ionicLoading, $ionicHistory, $timeout, $state, $globalUrl)
{ 
	
	var cp = sessionStorage.getItem('CouponId');
	if (cp == null)
	{
		$('#Applied-Coupon-Section').css('display', 'none');
	}
	else
	{
		$('#Applied-Coupon-Section').css('display', 'block');
	}



	$scope.ind = sessionStorage.getItem('all_Id');

	var cust = { "customerId": $scope.ind }
	var mystr = JSON.stringify(cust);
	$http.get($globalUrl + "OrderSummary?details=" + mystr).then(function (response)
	{
		$scope.totalcartPrice = response.data;
	});

	var cdetails = { "customerId": $scope.ind }
	var details = JSON.stringify(cdetails);

	$http.get($globalUrl + "Confirm?details=" + details).then(function (responsedata)
	{


		$scope.Loyalitydetails = responsedata.data.data.LoyaltyBox;
		$scope.checkattributes = responsedata.data.data.CheckoutAttributes;

	});


	setTimeout(function ()
	{
		$('#attributes4').prop('checked', true);		
	}, 1000);
	

	$scope.setselectedOption = function (optionId, optionvalueid)
	{
		$scope.selectedOption = optionId;
		$scope.selectedOptionValue = optionvalueid;

	};

	$scope.AlltibuteValBox = function (optionId, textBoxval)
	{
		$scope.chkAttOption = optionId;
		$scope.chkAttOptionValue = textBoxval;

		sessionStorage.setItem("attrchkAttOptionId", $scope.chkAttOption);
		sessionStorage.setItem("attrchkAttOptionValue", $scope.chkAttOptionValue);

	};


	
	$scope.ind = sessionStorage.getItem('all_Id');

	$scope.ITEMS = [];
	$http.get($globalUrl + "usershoppingcart/" + $scope.ind).then(function (response)
{
	$scope.cartItems = response.data;

	if (response.data.DiscountBox != undefined)
	{
		$scope.DiscountCode = JSON.stringify(response.data.data.DiscountBox.AppliedDiscountsWithCodes[0].CouponCode);//AppliedDiscountsWithCodes[0].CouponCode);
	}

	if ($scope.cartItems.success == true)
	{ 		
		angular.forEach($scope.cartItems.data.Items, function (value, key)
		{ 			
			$scope.ITEMS.push({ "ID": value.Id, "IMAGE": value.Picture.ImageUrl, "NAME": value.ProductName, "PRICE": value.UnitPrice, "QTY": value.Quantity, "TOTAL": value.SubTotal, "Reviews": value.TotalReviews, "CartItemId": value.Id, "ProductId": value.ProductId });
		})	
	
	}
	else
	{ 			
				window.location.href = "#/side-menu21/page6";
				$scope.ErrorMessage = $scope.cartItems.message;
				$('#lblAlertMessage').html($scope.cartItems.message);
				$('.alert').show();
				$('.alertBG').show();
		
	}
	

});


	//ConfirmOrder

	$scope.ConfirmOrder = function ()
	{

		$scope.custom_name = sessionStorage.getItem('name_user');

		if ($('#orderTextbox').val() == "")
		{
			$('#lblAlertMessage').html('Gevraagd Tijdstip is verplicht in te vullen');
			$('.alert').show();
			$('.alertBG').show();
		}
		else
		{

			if ($scope.custom_name == null)
			{
				sessionStorage.setItem("getCurrentPageUrl", "cartPage");

				sessionStorage.setItem("attributeVal", $scope.selectedOptionValue);

				if ($scope.chkAttOption == undefined || $scope.chkAttOption == 'undefined')
				{
					sessionStorage.setItem("attrchkAttOptionId", 3);
				}

				if ($scope.chkAttOptionValue == undefined || $scope.chkAttOptionValue == 'undefined')
				{
					sessionStorage.setItem("attrchkAttOptionValue", $('#orderTextbox').val());
				}

				window.location.href = "#/side-menu21/page4";
				$('#lblAlertMessage').html('U dient in te loggen om verder te gaan');
				$('.alert').show();
				$('.alertBG').show();

			}
			else
			{
				if ($scope.chkAttOption == undefined || $scope.chkAttOption == 'undefined')
				{
					$scope.chkAttOption = 3;
				}
				$scope.chkAttOptionValue = $('#orderTextbox').val();

				if ($scope.chkAttOptionValue == undefined || $scope.chkAttOptionValue == '')
				{
					$scope.chkAttOptionValue = 0;
				}

				if ($scope.selectedOptionValue == undefined || $scope.selectedOptionValue == 'undefined')
				{
					$scope.selectedOption = 2;
					$scope.selectedOptionValue = 4;
					window.location.href = "#/side-menu21/page32?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.chkAttOption + "&cChkAttOptionValue=" + $scope.chkAttOptionValue;
				}
				else if ($scope.selectedOptionValue == 4)
				{
					window.location.href = "#/side-menu21/page32?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.chkAttOption + "&cChkAttOptionValue=" + $scope.chkAttOptionValue;
				}
				else
				{
					window.location.href = "#/side-menu21/page33?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.chkAttOption + "&cChkAttOptionValue=" + $scope.chkAttOptionValue;
				}

				// //for test sewrver
				//if ($scope.selectedOptionValue == undefined || $scope.selectedOptionValue == 'undefined')
				//{
				//	$scope.selectedOption = 1;
				//	$scope.selectedOptionValue = 2;
				//	window.location.href = "#/side-menu21/page32?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.chkAttOption + "&cChkAttOptionValue=" + $scope.chkAttOptionValue;
				//	//window.location.href = "#/side-menu21/page32?cOptionValId="+ $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption;

				//}
				//else if ($scope.selectedOptionValue == 2)
				//{
				//	window.location.href = "#/side-menu21/page32?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.chkAttOption + "&cChkAttOptionValue=" + $scope.chkAttOptionValue;
				//	//window.location.href = "#/side-menu21/page32?cOptionValId="+ $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption;
				//}
				//else
				//{
				//	window.location.href = "#/side-menu21/page33?cOptionValId=" + $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption + "&cChkAttOption=" + $scope.chkAttOption + "&cChkAttOptionValue=" + $scope.chkAttOptionValue;
				//	//window.location.href = "#/side-menu21/page33?cOptionValId="+ $scope.selectedOptionValue + "&cOptionId=" + $scope.selectedOption;
				//}
			}
		}
	}


	// DELETE ITEM FROM CART
	$scope.del_item = function (CartItemId, productId)
	{

		var enteredQty = $('input#CQty').val();
		$scope.ind = sessionStorage.getItem('all_Id');
		$scope.cart_ID = sessionStorage.getItem('cartItem_id');
		$scope.shoppingCartType = 1;
		$scope.productId = sessionStorage.getItem('productId');

		if (enteredQty == 1 || enteredQty > 0)
		{
			var details = ({ EnteredQuantity: enteredQty, ShoppingCartType: 1, CustomerId: $scope.ind, productId: productId, cartItemId: CartItemId });

			var mystr = JSON.stringify(details);
			$ionicLoading.show();
			str = $globalUrl + "deleteItemsfromUsercart?details=" + mystr;
			$http.get(str)
			.success(function (response)
			{
				var result1 = JSON.stringify(response);
				$scope.ITEMS.length = 0;
				if (result1.success == false)
				{
					$('#lblAlertMessage').html('Er is een probleem opgetreden bij het verwijderen van het product');
					$('.alert').show();
					$('.alertBG').show();
				} 			
				location.reload();
				$ionicLoading.hide();

			})
		}
		else
		{
			$('#lblAlertMessage').html('Gelieve een hoeveelheid te selecteren');
			$('.alert').show();
			$('.alertBG').show();
		}
	}
	//End 



	$scope.increaseItemCount = function (enteredQty, CartItemId, productId)
	{
		enteredQty++;
		$scope.update_item(CartItemId, productId, enteredQty);

	};
	$scope.decreaseItemCount = function (enteredQty, CartItemId, productId)
	{
		if (enteredQty > 0)
		{
			enteredQty--;
			$scope.update_item(CartItemId, productId, enteredQty);
		}

	};


	// update ITEM FROM CART
	$scope.update_item = function (CartItemId, productId, enteredQty)
	{
		$scope.ind = sessionStorage.getItem('all_Id'); 		
		if (enteredQty == 1 || enteredQty > 0)
		{
			var details = ({ EnteredQuantity: enteredQty, ShoppingCartType: 1, CustomerId: $scope.ind, productId: productId, cartItemId: CartItemId });
			var mystr = JSON.stringify(details);
			$ionicLoading.show();
			str = $globalUrl + "UpdateCart?details=" + mystr;
			$http.get(str).success(function (response)
			{
				var result1 = JSON.stringify(response);
				$scope.ITEMS.length = 0;
				if (result1.success == false)
				{
					$('#lblAlertMessage').html('Er is een probleem bij het bijwerken van uw winkelwage');
					$('.alert').show();
					$('.alertBG').show();
				}
				else
				{
					$ionicLoading.hide();
					location.reload();
				}

			})
		}
		else
		{
			$('#lblAlertMessage').html('Gelieve een hoeveelheid te selecteren');
			$('.alert').show();
			$('.alertBG').show();
		}

	}
	//End

	//Coupon Code

	$scope.ApplyCoupon = function ()
	{
		var tDiscount = $('#tDiscount').val();

		$scope.ind = sessionStorage.getItem('all_Id');
		var couponcode = { DiscountCode: tDiscount, UserId: $scope.ind }

		var applyCoupon = JSON.stringify(couponcode);

		$scope.ITEMS = [];
		if (tDiscount != "")
		{
			$ionicLoading.show();
			$.when($http.get($globalUrl + "ApplyCoupon?details=" + applyCoupon).success(function (response)
			{
				var couponStatus = response.success;
				if (couponStatus)
				{
					$scope.CouponId = response.data.Id;				
					$scope.DiscountCode = tDiscount;
					location.reload();
				}
				else
				{
					$ionicLoading.hide();
					$('#lblAlertMessage').html('Kortingscode niet geldig');
					$('.alert').show();
					$('.alertBG').show();
				}

			})).then(function (data, textStatus, jqXHR)
			{
				$ionicLoading.hide();
				$('#lblAlertMessage').html(response.message);
				$('.alert').show();
				$('.alertBG').show();
				location.reload();
			});
		}
		else
		{
			$('#lblAlertMessage').html('Gelieve uw kortingscode in te voeren');
			$('.alert').show();
			$('.alertBG').show();
		}

	}

	$scope.RemoveCoupon = function ()
	{
		$scope.ind = sessionStorage.getItem('all_Id');
		$scope.tDiscount = $('#dicountCode').val(); 
		var couponcode = { Id: $scope.tDiscount, UserId: $scope.ind }
		var applyCoupon = JSON.stringify(couponcode);
		$ionicLoading.show();

		$.when($http.get($globalUrl + "DeleteCoupons?details=" + applyCoupon).success(function (response)
		{

		})).then(function (data, textStatus, jqXHR)
		{
			sessionStorage.removeItem('CouponId');
			$ionicLoading.hide();
			location.reload();
		});

	}

	$scope.ApplyLoyality = function ()
	{
		var PointsToUse = $('#tLoyality').val();
		$scope.ind = sessionStorage.getItem('all_Id');
		var loyality = { PointsToUse: PointsToUse, UserId: $scope.ind }
		var loyality2 = JSON.stringify(loyality);


		if (PointsToUse == null || PointsToUse == "")
		{
			$('#lblAlertMessage').html("Geef uw loyalty punten in");
			$('.alert').show();
			$('.alertBG').show();
		}
		else
		{

			$http.get($globalUrl + "ApplyLoyalityPoints?details=" + loyality2).success(function (response)
			{
				var Status = response.success;
				if (Status)
				{
					$scope.LoyalityId = PointsToUse;
					sessionStorage.setItem('Loyality', PointsToUse);
					$('#tLoyality').val('');
					$ionicLoading.hide();
					location.reload();
				}
				else
				{
					$('#lblAlertMessage').html(response.message);
					$('.alert').show();
					$('.alertBG').show();
				}

			}).then(function (data, textStatus, jqXHR)
			{
				$('#lblAlertMessage').html(response.message);
				$('.alert').show();
				$('.alertBG').show();
				$ionicLoading.hide();
				location.reload();
			});
		}




	}

	$scope.RemoveLoyality = function ()
	{
		var PointsToUse = $('#tLoyality').val();
		$scope.ind = sessionStorage.getItem('all_Id');
		$scope.tloyal = sessionStorage.getItem('Loyality');
		var loyality = { PointsToUse: $scope.tloyal, UserId: $scope.ind }
		var loyality2 = JSON.stringify(loyality);
		$ionicLoading.show();
		$.when($http.get($globalUrl + "DeleteLoyalityPoints?details=" + loyality2).success(function (response)
		{

		})).then(function (data, textStatus, jqXHR)
		{
			sessionStorage.removeItem('Loyality');
			$ionicLoading.hide();
			location.reload();
		});
	}

	//END


	$scope.showPDetail = function (id)
	{
		window.location.href = "#/side-menu21/page3?productId=" + id;
	}


	$scope.GoToHome = function ()
	{
		window.location.href = "#/side-menu21/page1";
	}


}])

.controller('newsCtrl', ['$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $timeout, $ionicLoading, $state, $globalUrl)
{

	$scope.$on('$ionicView.enter', function ()
	{
		$scope.currentPage = 0;
		$scope.pageSize = 4;

		$ionicLoading.show();
		$scope.ITEMS = [];
		$http.get($globalUrl + "NewsList")
			 .then(function (response)
			 {
			 	$scope.newsData = response.data;

			 	angular.forEach($scope.newsData.data.NewsItems, function (value, key)
			 	{
			 		$scope.ITEMS.push({ "ID": value.Id, "TITLE": value.Title, "DESC": value.Short, "Image": value.PictureModel.ImageUrl });

			 	})
			 	$ionicLoading.hide();
			 });

		$scope.numberOfPages = function ()
		{
			return Math.ceil($scope.ITEMS.length / $scope.pageSize);
		}

	});


	//News Detail 
	$scope.newsDetail = function (Newsid)
	{ 		
		window.location.href = "#/side-menu21/page27?NewsId=" + Newsid;
	}
	//End



}])



.controller('newsDetailsCtrl', ['$location', '$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $timeout, $ionicLoading, $globalUrl)
{

	$scope.$on('$ionicView.enter', function ()
	{			
		$scope.Newsid = ($location.search()).NewsId;	
		$http.get($globalUrl + "GetNewsDetails/" + $scope.Newsid).success(function (response)
		{
			if (response.success == false)
			{
				//alert(response.message);
				$('#lblAlertMessage').html(response.message);
				$('.alert').show();
				$('.alertBG').show();
			}
			else
			{
				$scope.newsDetailData = response.data;	 				
			}			
		});

	});


}])




.controller('blogCtrl', ['$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $timeout, $ionicLoading, $state, $globalUrl)
{


	$scope.$on('$ionicView.enter', function ()
	{
		$scope.currentPage = 0;
		$scope.pageSize = 4;


		$ionicLoading.show();
		$scope.ITEMS = [];
		$http.get($globalUrl + "BlogsList")
			 .then(function (response)
			 {
			 	$scope.blogData = response.data;
			 	angular.forEach($scope.blogData.data.BlogPosts, function (value, key)
			 	{
			 		$scope.ITEMS.push({ "ID": value.Id, "TITLE": value.Title, "DESC": value.BodyOverview, "Image": value.PictureModel.ImageUrl });

			 	})
			 	$ionicLoading.hide();
			 });

		$scope.numberOfPages = function ()
		{
			return Math.ceil($scope.ITEMS.length / $scope.pageSize);
		}

	});

	//Blog Detail 
	$scope.blogDetail = function (Blogid)
	{ 		
		window.location.href = "#/side-menu21/page28?blogId=" + Blogid;  		
	}
	//End

}])


.controller('blogDetailsCtrl', ['$location', '$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $timeout, $ionicLoading, $state, $globalUrl)
{

	$scope.Blogid = ($location.search()).blogId;

	$scope.$on('$ionicView.enter', function ()
	{ 	
		$ionicLoading.show();

		$http.get($globalUrl + "GetBlogDetails/" + $scope.Blogid).success(function (response)
		{
			if (response.success == false)
			{				
				$('#lblAlertMessage').html(response.message);
				$('.alert').show();
				$('.alertBG').show();
			}
			else
			{
				$scope.BlogDetailData = response.data;	 				
			}
			$ionicLoading.hide();
		});

	});



}])



.controller('paymentCtrl', ['$location', '$scope', '$stateParams', '$location', '$http', '$state', '$timeout', '$globalUrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $location, $http, $state, $timeout, $globalUrl)
{
	$('#payment-submit').css('display', 'none');
	$scope.Orderid = ($location.search()).orderId;

	$scope.ind = sessionStorage.getItem('all_Id');


	if ($scope.ind == undefined || $scope.ind == null || $scope.ind == 'undefined')
	{
		$scope.ind = localStorage.getItem("all_Id");
	}

	//Added 'a' char for Android
	$http.get($globalUrl + "PlaceOnConfirma/" + $scope.Orderid)
	.then(function (response)
	{
		$scope.placeResult = response.data;
		$scope.Seal = response.data.Seal;
		$scope.sendData = response.data.data;	 		
		$scope.resMessage = response.message;

		setTimeout(function ()
		{
			$('#payment-submit').trigger('click');
		}, 1500);



	});


	//setTimeout(function ()
	//{
	// $('#payment-submit').trigger('click');
	//}, 5000);
	//$('#payment-submit').css('display', 'none');

	$scope.GoHome = function ()
	{
		window.location.href = "#/side-menu21/page1";
	}


}])


.controller('placeOrderCtrl', ['$scope', '$stateParams', '$http', '$ionicHistory', '$state', '$ionicLoading', '$timeout', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicHistory, $state, $ionicLoading, $timeout, $globalUrl)
{


	$scope.$on('$ionicView.enter', function ()
	{
		$ionicLoading.show();
		$scope.id = sessionStorage.getItem('order_info_id');
		$scope.mydata = [];

		$.get($globalUrl + "GetOrderDetails/" + $scope.id, function (data)
		{
			$scope.trest = data;
			angular.forEach($scope.trest.Items, function (value, key)
			{
				$scope.cart_product = value;
				$scope.mydata.push({ "ID": value.Id, "PRODUCTNAME": value.ProductName, "PICTUREURL": value.PictureThumbnailUrl, "QUANTITY": value.Quantity, "SUBTOTAL": value.SubTotal, "ORDERSTATUS": value.OrderStatus, "ORDERTOTAL": value.OrderTotal });

			})
			$ionicLoading.hide();
		});
	});
	//END


	$scope.reorder = function ()
	{
		$scope.id = sessionStorage.getItem('order_info_id');
		$scope.ind = sessionStorage.getItem('all_Id');
		$ionicLoading.show();
		$http.get($globalUrl + "Reorder/" + $scope.id + "/" + $scope.ind)
	.then(function (response)
	{
		$scope.reorderResponse = response.data;

		if ($scope.reorderResponse.success == true)
		{
			$('#lblAlertMessage').html($scope.reorderResponse.message);
			$('.alert').show();
			$('.alertBG').show();
		}
		else
		{	 		
			$('#lblAlertMessage').html($scope.reorderResponse.message);
			$('.alert').show();
			$('.alertBG').show();
		}

		$ionicLoading.hide();
	});

	}

	$scope.returnorder = function ()
	{
		window.location.href = "#/side-menu21/page23";
	}

	//GO TO HOME PAGE
	$scope.homeLogo = function ()
	{
		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		$state.go('menu.home');
	}


}])


.controller('confirmOrderCtrl', ['$location', '$scope', '$stateParams', '$http', '$ionicHistory', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $ionicHistory, $state, $globalUrl)
{
	$scope.ind = sessionStorage.getItem('all_Id');
	$scope.custom_name = sessionStorage.getItem('name_user');


	$scope.AddressId = 0;
	$scope.AddressId = ($location.search()).AddressId;


	if ($scope.AddressId == undefined || $scope.AddressId == "undefined")
	{
		$scope.AddressId = 0;
	}	 

	$scope.optionId = ($location.search()).cOptionId;
	$scope.optionValueId = ($location.search()).cOptionValId;

	//TextBox Id
	$scope.cChkAttOption = ($location.search()).cChkAttOption;
	//TextBox value
	$scope.cChkAttOptionValue = ($location.search()).cChkAttOptionValue;

	if ($scope.optionValueId == 4)
	{
		$scope.Delivery = "Afhaling in OX";
	}
	else
	{
		$scope.Delivery = "Levering";
	}

	var CustomerId = { customerId: $scope.ind };
	var ordDetail = JSON.stringify(CustomerId);

	if ($scope.custom_name == "")
	{
		$('#lblAlertMessage').html('U dient in te loggen om verder te gaan');
		$('.alert').show();
		$('.alertBG').show();
		window.location.href = "#/side-menu21/page4";
	}
	else
	{
		angular.element(document).ready(function ()
		{

			$scope.CartITEMS = [];
			$http.get($globalUrl + "usershoppingcart/" + $scope.ind)
			.then(function (response)
			{
				$scope.cartItems = response.data;
				if ($scope.cartItems.success == true)
				{
					angular.forEach($scope.cartItems.data.Items, function (value, key)
					{
						$scope.CartITEMS.push({ "ID": value.Id, "IMAGE": value.Picture.ImageUrl, "NAME": value.ProductName, "PRICE": value.UnitPrice, "QTY": value.Quantity, "TOTAL": value.SubTotal, "Reviews": value.TotalReviews, "CartItemId": value.Id, "ProductId": value.ProductId });
					})
				}
			});

			$http.get($globalUrl + "OrderSummary?details=" + ordDetail)
		.then(function (response)
		{
			$scope.totalprice = response.data;					  
		});


		});

		$scope.placeOrder = function ()
		{

			$scope.ind = sessionStorage.getItem('all_Id');

			var ordersnt = {
				customerId: $scope.ind, ShippingAddressID: $scope.AddressId, "CheckoutAttributeValues": "CheckOutAttributeId:" + $scope.optionId + ",CheckOutAttributeValueId:" + $scope.optionValueId + ";CheckOutAttributeId:" + $scope.cChkAttOption + ",CheckOutAttributeValueId:" + $scope.cChkAttOptionValue + ""
			} 		
			var ordersentJson = JSON.stringify(ordersnt);			
			$http.get($globalUrl + "checkoutprocess?details=" + ordersentJson)
	.then(function (response)
	{
		$scope.finalorder_id = response.data;
		window.location.href = "#/side-menu21/page11?orderId=" + $scope.finalorder_id;
		location.reload();
		$state.reload();


	});

		}


	}
}])


.controller('myOrdersCtrl', ['$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$ionicHistory', '$state', '$globalUrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $timeout, $ionicLoading, $ionicHistory, $state, $globalUrl)
{


	$scope.$on('$ionicView.enter', function ()
	{
		$ionicLoading.show();
		$scope.ind = sessionStorage.getItem('all_Id');
		$scope.ordersdata = [];

		$.get($globalUrl + "GetCustomerOrders/" + $scope.ind, function (response)
		{
			$scope.trest = response.data;
			angular.forEach($scope.trest.Orders, function (value, key)
			{
				sessionStorage.setItem('order_info_id', value.Id);	  				
				var CreatedOn = (value.CreatedOn.replace('/Date(', ''));
				var CreatedOn2 = CreatedOn.replace(')/', '');
				$scope.ordersdata.push({ "ID": value.Id, "PAYMENTSTATUS": value.PaymentStatus, "ORDERSTATUS": value.OrderStatus, "ORDERTOTAL": value.OrderTotal, "CREATEDON": CreatedOn2 });

			})
			$ionicLoading.hide();
		});

	});


	//start for view order detail 
	$scope.showOrderInfo = function (id)
	{
		sessionStorage.setItem('order_info_id', id);
		window.location.href = "#/side-menu21/page17";
	}
	//End


	//GO TO HOME PAGE
	$scope.homeLogo = function ()
	{
		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		$state.go('menu.home');
	}


}])

 .controller('orderHistoryCtrl', ['$scope', '$stateParams', '$http', '$ionicHistory', '$state', '$ionicLoading', '$timeout', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicHistory, $state, $ionicLoading, $timeout, $globalUrl)
{


	$scope.$on('$ionicView.enter', function ()
	{
		$ionicLoading.show();

		$scope.id = sessionStorage.getItem('order_info_id');
		$scope.myorderdata = [];

		$.get($globalUrl + "GetOrderDetails/" + $scope.id, function (response)
		{
			$scope.orderData = response.data;
			var CreatedOn = (response.data.CreatedOn.replace('/Date(', ''));
			var CreatedOn2 = CreatedOn.replace(')/', '');
			$scope.orderData.CreatedOn = CreatedOn2;
			angular.forEach($scope.orderData.Items, function (value, key)
			{ 							
				$scope.myorderdata.push({ "ID": value.Id, "PRODUCTNAME": value.ProductName, "PRICE": value.UnitPrice, "PICTUREURL": value.PictureThumbnailUrl, "QUANTITY": value.Quantity, "SUBTOTAL": value.OrderSubtotal, "ORDERSTATUS": value.OrderStatus, "ORDERTOTAL": value.OrderTotal });

			})
			$ionicLoading.hide();
		});

	});
	//END


	$scope.reorder = function (OrderId)
	{
		$scope.ind = sessionStorage.getItem('all_Id');
		$ionicLoading.show();
		$http.get($globalUrl + "Reorder/" + OrderId + "/" + $scope.ind)
	.then(function (response)
	{
		$scope.reorderResponse = response.data;

		if ($scope.reorderResponse.success == true)
		{ 			
			$('#lblAlertMessage').html($scope.reorderResponse.message);
			$('.alert').show();
			$('.alertBG').show();
		}
		else
		{ 			
			$('#lblAlertMessage').html($scope.reorderResponse.message);
			$('.alert').show();
			$('.alertBG').show();
		}
		$ionicLoading.hide();
	});

	}


	$scope.retryOrder = function (OrderId)
	{
		window.location.href = "#/side-menu21/page11?orderId=" + OrderId;
		location.reload();
			$state.reload();
		//var ref = window.open("#/side-menu21/page11?orderId=" + OrderId, "_blank", "location=yes,clearcache=yes,toolbar=no");//closebuttoncaption=Back to OXCFE

		//ref.addEventListener('loadstop', function (event)
		//{
		//	if (event.url.indexOf("gotomobile") >= 0)
		//	{
		//		setTimeout(function ()
		//		{
		//			ref.close();
		//			window.location.href = "#/side-menu21/page1";
		//		},
		//	  5000);
		//	}
		//});
	}



	//GO TO HOME PAGE
	$scope.homeLogo = function ()
	{
		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		$state.go('menu.home');
	}

	//go to product page
	$scope.GotoProductDetail = function (id)
	{
		sessionStorage.removeItem('product_detail_id');
		sessionStorage.setItem('product_detail_id', id);
		window.location.href = "#/side-menu21/page3";
	}


}])



.controller('orderCtrl', ['$scope', '$stateParams', '$ionicLoading', '$http', '$timeout', '$globalUrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $ionicLoading, $http, $timeout, $globalUrl)
{

	$scope.$on('$ionicView.enter', function ()
	{
		$scope.ind = sessionStorage.getItem('all_Id');
		$ionicLoading.show();
		$scope.ITEMS = [];
		$http.get($globalUrl + "GetCustomerOrders/" + $scope.ind)
	.then(function (response)
	{
		$scope.orderItems = response.data;

		if ($scope.orderItems.data.Orders.length == 0 || $scope.orderItems.data.Orders.length == null)
		{			
			$('#lblAlertMessage').html('Geen bestelling beschikbaar');
			$('.alert').show();
			$('.alertBG').show();
			window.location.href = "#/side-menu21/page2";
		}
		else
		{
			angular.forEach($scope.orderItems.Orders, function (value, key)
			{
				$scope.ITEMS.push({ "ID": value.Id, "ORDERTOTAL": value.OrderTotal, "DATE": value.CreatedOn, "ORDERSTATUS": value.OrderStatus });
			})
			$ionicLoading.hide();
		}
	});

	});

}])


.controller('emailFriendCtrl', ['$location', '$scope', '$stateParams', '$http', '$ionicPopup', '$state', '$ionicHistory', '$timeout', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $ionicPopup, $state, $ionicHistory, $timeout, $globalUrl)
{

	$scope.ind = sessionStorage.getItem('all_Id');
	if ($scope.ind == null || $scope.ind == "undefined" || $scope.ind == undefined)
	{
		$scope.loginId = localStorage.getItem("all_Id");

		if ($scope.loginId == null || $scope.loginId == "undefined" || $scope.loginId == undefined)
		{

			$scope.ind = 0;
		}
		else
		{
			$scope.ind = $scope.loginId;
		}

	}

	$scope.productId = sessionStorage.getItem('product_detail_id');

	$scope.EmailToFriend = function (data)
	{
	
		var json = { ProductId: $scope.productId, CustomerId: $scope.ind, YourEmailAddress: data.YourEmailAddress, FriendEmail: data.FriendEmail, PersonalMessage: data.PersonalMessage };

		var mystr = JSON.stringify(json);	
		var link = $globalUrl + "productsendtofriend?details=" + mystr;

		$http.get(link, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
		.then(function (res)
		{
			$scope.response = res.data;

			if ($scope.response.success == true)
			{
				$('#lblAlertMessage').html('Email met succes verstuurd');
				$('.alert').show();
				$('.alertBG').show();
				sessionStorage.setItem('product_detail_id', $scope.productId);
				window.location.href = "#/side-menu21/page3";

			}
			else if ($scope.response.success == false)
			{
				$('#lblAlertMessage').html($scope.response.message);
				$('.alert').show();
				$('.alertBG').show();
			}

		});
		//}

	}

}])


.controller('productReviewsCtrl', ['$location', '$scope', '$stateParams', '$http', '$ionicPopup', '$state', '$ionicHistory', '$timeout', '$globalUrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $ionicPopup, $state, $ionicHistory, $timeout, $globalUrl)
{

	$scope.ind = sessionStorage.getItem('all_Id');	
	$scope.productId = ($location.search()).productId;	 
	$scope.$on('$ionicView.enter', function ()
	{

		$scope.reviewsData = [];  	
		$http.get($globalUrl + "getproductreviews/" + $scope.productId + "/" + $scope.ind)
 	  .then(function (response)
 	  {

 	  	if (response.data.success == true)
 	  	{
 	  		$scope.reviewItems = response.data.data;

 	  		angular.forEach($scope.reviewItems.Items, function (value, key)
 	  		{
 	  			$scope.reviewsData.push({ "ID": value.Id, "Title": value.Title, "Rating": value.Rating, "ReviewText": value.ReviewText, "CustomerName": value.CustomerName, "WrittenOnStr": value.WrittenOnStr });
 	  		})		  	  	
 	  	}
 	  	else
 	  	{;
 	  		$scope.resMessage = response.data.message;
 	  	}


 	  });

	});




	$scope.AddReviews = function (data)
	{

		var detailsData = { ProductId: $scope.productId, UserId: $scope.ind, TTitle: data.ReviewTitle, TReviewText: data.ReviewText, Rating: "1" };	  
		var myData = JSON.stringify(detailsData);		
		var link = $globalUrl + "AddReviews?details=" + myData;
		$http.get(link, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
		.then(function (res)
		{
			$scope.response = res.data;

			if ($scope.response.success == true)
			{
				$('#lblAlertMessage').html('Reviews met succes toegevoegd');
				$('.alert').show();
				$('.alertBG').show();
				location.reload();
			}
			else if ($scope.response.success == false)
			{
				$('#lblAlertMessage').html($scope.response.message);
				$('.alert').show();
				$('.alertBG').show();
			}

		});
		
	}




}])



.controller('shipbillCtrl', ['$location', '$scope', '$stateParams', '$http', '$state', '$ionicHistory', '$globalUrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $state, $ionicHistory, $globalUrl)
{
	$scope.ind = localStorage.getItem('all_Id');
	$scope.custom_name = sessionStorage.getItem('name_user');


	$scope.optionId = ($location.search()).cOptionId;

	if ($scope.optionId == undefined || $scope.optionId == 'undefined')
		$scope.optionId = 0;

	$scope.optionValueId = ($location.search()).cOptionValId;
	if ($scope.optionValueId == undefined || $scope.optionValueId == 'undefined')
		$scope.optionValueId = 0;


	//TextBox Id
	$scope.cChkAttOption = ($location.search()).cChkAttOption;
	//TextBox value
	$scope.cChkAttOptionValue = ($location.search()).cChkAttOptionValue;



	if ($scope.custom_name == "")
	{
		$('#lblAlertMessage').html('Gelieve in te loggen om uw bestelling af te ronden');
		$('.alert').show();
		$('.alertBG').show();
		window.location.href = "#/side-menu21/page4";
	}
	else
	{
		angular.element(document).ready(function ()
		{
			$scope.mydata = []

			$http.get($globalUrl + "customerInformation/" + $scope.ind)
		 .then(function (response)
		 {
		 	$scope.address = response.data.data;  		 	
		 	$scope.AddressData = $scope.address.ExistingAddresses;

		 	var Add1 = $scope.address.ExistingAddresses[0];
		 	$('#customerInformation-address1').val(Add1.Address1);
		 	$('#customerInformation-addres2').val(Add1.Address2);
		 	$('#customerInformation-email').val(Add1.Email);
		 	$('#customerInformation-city').val(Add1.City);
		 	$('#customerInformation-zipcode').val(Add1.ZipPostalCode);
		 	$('#customerInformation-phone').val(Add1.PhoneNumber);
		 	angular.forEach($scope.address.ExistingAddresses, function (value, key)
		 	{		 	
		 		$scope.selectedAddress = value.Id;
		 	});


		 });
		});


		$scope.check = function (id)
		{

			$scope.selectedAddress = id;
		}

		//Add new Address and update user information

		$scope.updateAddress = function ()
		{
			$scope.addressUser = sessionStorage.getItem('address_Id');

			var PhoneNumber = $('#customerInformation-phone').val();
			var Address1 = $('#customerInformation-address1').val();
			var Address2 = $('#customerInformation-addres2').val();
			var City = $('#customerInformation-city').val();  	
			var ZipPostalCode = $('#customerInformation-zipcode').val();		
			var Email = $('#customerInformation-email').val();
			var Fax = $('#customerInformation-FaxNumber').val();

			if ($scope.addressUser == null || $scope.addressUser == "")
			{
				var detailsdata = { Id: $scope.ind, PhoneNumber: PhoneNumber, Address1: Address1, Address2: Address2, City: City, ZipPostalCode: ZipPostalCode, Email: Email, CountryId: "12" };
				var mystring = JSON.stringify(detailsdata);				
				var link = $globalUrl + "newcustomeraddress?json=" + mystring;
				$http.get(link)
				.success(function (res)
				{
					$('#lblAlertMessage').html(res.message);
					$('.alert').show();
					$('.alertBG').show();
				});
				$scope.selectedAddress = response.data.id;
			}

			else
			{

				var json = { Id: $scope.addressUser, PhoneNumber: PhoneNumber, Address1: Address1, Address2: Address2, City: City, ZipPostalCode: ZipPostalCode, Email: Email, CountryId: "12" };
				var mystr = JSON.stringify(json); 			
				var link = $globalUrl + "editcustomeraddress?json=" + mystr;
				$http.get(link)
				.success(function (res)
				{
					$('#lblAlertMessage').html(res.message);
					$('.alert').show();
					$('.alertBG').show();
				});
				$scope.selectedAddress = response.data.id;
			}


		}

		//End


		$scope.GotoConfirm = function ()
		{
			if ($scope.selectedAddress != null)
			{
				var address1 = $('#customerInformation-address1').val();
				var emailAdd = $('#customerInformation-email').val();

				if (address1 == '' || emailAdd == '')
				{
					$('#lblAlertMessage').html('Gelieve uw adres te selecteren');
					$('.alert').show();
					$('.alertBG').show();
				}
				else
				{
					window.location.href = "#/side-menu21/page32?AddressId=" + $scope.selectedAddress + "&cOptionValId=" + $scope.optionValueId + "&cOptionId=" + $scope.optionId + "&cChkAttOption=" + $scope.cChkAttOption + "&cChkAttOptionValue=" + $scope.cChkAttOptionValue;
				}
			}
			else
			{
				$('#lblAlertMessage').html('Gelieve uw adres te selecteren');
				$('.alert').show();
				$('.alertBG').show();
			}
		}

		//GO TO HOME PAGE
		$scope.homeLogo = function ()
		{
			$ionicHistory.nextViewOptions({
				disableBack: true
			});

			$state.go('menu.home');
		}
	}

}])



.controller('changePWDCtrl', ['$scope', '$stateParams', '$http', '$ionicPopup', '$state', '$ionicHistory', '$timeout', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup, $state, $ionicHistory, $timeout, $globalUrl)
{
	$scope.ind = sessionStorage.getItem('all_Id');
	if ($scope.ind == null || $scope.ind == "undefined" || $scope.ind == undefined)
	{
		$scope.loginId = localStorage.getItem("all_Id");

		if ($scope.loginId == null || $scope.loginId == "undefined" || $scope.loginId == undefined)
		{

			$scope.ind = 0;
		}
		else
		{
			$scope.ind = $scope.loginId;
		}

	}

	$scope.ChangePWD = function (data)
	{

		var json = { OldPassword: data.OldPassword, NewPassword: data.NewPassword, ConfirmNewPassword: data.ConfirmNewPassword, CustomerId: $scope.ind };
		var mystr = JSON.stringify(json); 	
		var link = $globalUrl + "changepassword?details=" + mystr;
		$http.get(link, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
		 .then(function (res)
		 {
		 	$scope.response = res.data;

		 	if ($scope.response.success == true)
		 	{ 		 	
		 		$('#lblAlertMessage').html($scope.response.message);
		 		$('.alert').show();
		 		$('.alertBG').show();

		 	}
		 	else
		 	{
		 		$('#lblAlertMessage').html($scope.response.message);
		 		$('.alert').show();
		 		$('.alertBG').show();
		 	}


		 });

	}

}])


.controller('disclaimerCtrl', ['$location', '$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $timeout, $ionicLoading, $state, $globalUrl)
{

	$scope.$on('$ionicView.enter', function ()
	{
		$ionicLoading.show();
		$http.get($globalUrl + "GetTopicsdetails/12").success(function (response)
		{
			if (response.success == false)
			{
				$('#lblAlertMessage').html(response.message);
				$('.alert').show();
				$('.alertBG').show();
			}
			else
			{
				$scope.disclaimerData = response.data;
			}
			$ionicLoading.hide();
		});

	});


}])




.controller('aboutCtrl', ['$location', '$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $timeout, $ionicLoading, $state, $globalUrl)
{


	$scope.$on('$ionicView.enter', function ()
	{	
		$ionicLoading.show();

		$http.get($globalUrl + "GetTopicsdetails/1").success(function (response)
		{
			if (response.success == false)
			{
				$('#lblAlertMessage').html(response.message);
				$('.alert').show();
				$('.alertBG').show();
			}
			else
			{
				$scope.aboutusData = response.data;
			}
			$ionicLoading.hide();
		});

	});



}])




.controller('disclaimerCtrl', ['$location', '$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$state', '$globalUrl', // The following is the constructor function for this page's  controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $timeout, $ionicLoading, $state, $globalUrl)
{

	$scope.$on('$ionicView.enter', function ()
	{
		$ionicLoading.show();
		$http.get($globalUrl + "GetTopicsdetails/12").success(function (response)
		{
			if (response.success == false)
			{
				$('#lblAlertMessage').html(response.message);
				$('.alert').show();
				$('.alertBG').show();
			}
			else
			{
				$scope.disclaimerData = response.data;
			}
			$ionicLoading.hide();
		});

	});


}])




.controller('aboutCtrl', ['$location', '$scope', '$stateParams', '$http', '$timeout', '$ionicLoading', '$state', '$globalUrl', // The following is the constructor function for this page's controller. See  	https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($location, $scope, $stateParams, $http, $timeout, $ionicLoading, $state, $globalUrl)
{


	$scope.$on('$ionicView.enter', function ()
	{

		$ionicLoading.show();

		$http.get($globalUrl + "GetTopicsdetails/1").success(function (response)
		{
			if (response.success == false)
			{
				$('#lblAlertMessage').html(response.message);
				$('.alert').show();
				$('.alertBG').show();
			}
			else
			{
				$scope.aboutusData = response.data;
			}
			$ionicLoading.hide();
		});

	});



}])


// To Generate GUID
function guid()
{
	function s4()
	{
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	  s4() + '-' + s4() + s4() + s4();
}




