angular.module('app.routes', ['ionic', 'pascalprecht.translate'])

.config(function ($stateProvider, $urlRouterProvider, $translateProvider)
{
	$translateProvider.translations('en', {
		
		nav_news_promo: "NEWS & PROMO",
		nav_blog: "BLOG",
		nav_cart: "CART",
		nav_login: "LOGIN",

		//menu
		menu_myaccount: "MAIN ACCOUNT",
		menu_ox_membership: "OX MEMBERSHIP",
		menu_my_order: "MY ORDERS",
		menu_shopping_cart: "SHOPPING CART",
		menu_news_promo: "NEWS & PROMO",
		menu_blog: "BLOG",
		menu_login: "LOGIN?",
		menu_logout: "LOG OUT",

		//HOME page
		lbl_day_offers: "DAY OFFERS",
		btn_to_order: "TO ORDER",
		lbl_lunch: "LUNCH",
		lbl_drinks: "DRINKS",

		//Login Page
		lbl_login: "LOGIN",
		placeholder_uname: "Username",
		placeholder_pwd: "Password",
		btn_remember_me: "Remember me?",
		link_forgot_pwd: "Forgot your password?",
		btn_login: "Login",
		btn_register: "Register",

		//Register Page
		lbl_welcome: "Welcome",
		lbl_create_an_acc: "Create an account",
		err_fname_req: "First Name is required",
		err_lname_req: "Last Name is required",
		err_email_req: "Enter valid emailId.",
		lbl_new_user: "New User?",
		placeholder_fname: "First Name",
		placeholder_lname: "Last Name",
		placeholder_email: "Email",
		btn_signup: "Sign Up",
		lbl_agreement_text: "By creating an account, you agree to Ox, Condition of Use and Privacy",

		//News & promo
		lbl_news: "NEWS",

		//Shopping Cart
		lbl_my_shoppimh_cart: "My Shopping Cart",
		lbl_products: "Product(s)",
		lbl_price: "PRICE",
		lbl_unit_price: "UNIT PRICE:",
		lbl_total_price: "TOTAL PRICE:",
		lbl_discount: "Discount",
		lbl_total: "Total",
		lbl_you_will_earn: "You will earn",
		btn_complete_order: "Complete order",
		lbl_loyality_pts: "Loyalty Points",
		lbl_loyality_pts_use: "Loyalty points to use(-)",
		lbl_discount_code: "Discount code",
		btn_to_apply: "To apply",

		lbl_use_my_reward_pts: "Use my reward points, ",
		lbl_availble: " available ",
		lbl_coupn_applied: "The coupon code has been applied",
		lbl_enter_cc: "Entered discount code",

		//blog and blog detail
		lbl_blogtitle: "BLOG",

		//Change password
		lbl_chnageheading: "MAIN ACCOUNT - Change Password",
		lbl_Olpass: "Old password:",
		lbl_newpass: "New password:",
		lbl_confirmnewpass: "Confirm password:",

		lbl_Olpassvalidate: "Old password is required",
		lbl_newpassvalidate: "New password is required",
		lbl_confirmnewpassvalidate: "Confirm Password is required",
		lbl_passbutton: "Change password",
		lbl_youremail: "Your email address",
		lbl_reorder: "Order again",

		//Confirm Order
		lbl_confirmorder: "Confirm your order",
		lbl_confirmbutton: "Confirm Order",
		lbl_payment: "Payment",
		lbl_paymentmethod: "Payment Method:",
		lbl_worldline: "Worldline",
		lbl_rewardpoints: "reward points",
		lbl_youwill: "You will earn",
		lbl_points: "points",
		lbl_loyaltybalance: "Loyalty Points Balance:",


		//Commom
		lbl_readmore: "READ MORE",
		lbl_ordersummary: "Order Summary",
		lbl_product: "PRODUCT",
		lbl_qty: "QTY",
		lbl_total: "TOTAL",
		lbl_desc: "DESCRIPTION",

		//email

		lbl_friendemail: "Email a friend",
		lbl_friend: "Friend's email",
		lbl_required: "Required!",
		lbl_notvalidemail: "Not a valid email!",
		lbl_message: "Personal message",
		lbl_sendbutton: "Send Email to friend",

		//my orders	 and orderhistory
		lbl_myorder: "MY ORDERS",
		lbl_ordernumber: "Order number:",
		lbl_orderdate: "Order Date:",
		lbl_orderstatus: "Order Status:",
		lbl_paymentstatus: "Payment Status:",
		lbl_ordertotal: "Order Total:",
		lbl_vieworder: "View order detail",
		lbl_noOrder: "No Orders placed yet",
		lbl_orderhistory: "Order History",
		lbl_order: "Order#",
		lbl_retry:"Retry",

		//productdetail
		lbl_prodsdetail: "PRODUCT DETAIL",
		lbl_maylike: "You also may like",

		lbl_prodreviews: "Product reviews",
		lbl_writereview: "Write your own review",
		lbl_reviewtitle: "Review Title",
		lbl_rvwtitlereq: "Review Title is Required.",
		lbl_rvwtext: "Review Text:",
		lbl_rating: "Rating:",
		lbl_submitrvw: "Submit review",
		lbl_exitrvw: "Existing reviews",
		lbl_bad: "Bad",
		lbl_exc: "Excellent",
		lbl_from: "From:",
		lbl_date: "Date:",

		//profile
		lbl_profile: "PROFILE",
		lbl_email: "Email:",
		lbl_name: "Name:",
		lbl_personal: "Personal number:",
		lbl_company: "Company:",

		//shipbill
		lbl_shipto: "Ship To:",
		lbl_selectAdd: "Select Address",
		lbl_add1_required: "Address 1 is required.",
		lbl_email_required: "Email is required.",
		lbl_city_required: "City is required.",
		lbl_zip_required: "Zip is required.",
		lbl_phone_required: "Phone is required.",
		btn_go_confirm: "Go to Confirm Order",
		btn_save_and_update_add: "Save and Update Address"
	});
	$translateProvider.translations('be', {	
		
		nav_news_promo: "NIEUWS & PROMO",
		nav_cart: "CART",
		nav_login: "LOG IN",
		nav_blog: "BLOG",

		//menu
		menu_myaccount: "MIJN ACCOUNT",
		menu_ox_membership: "OX MEMBERSHIP",
		menu_my_order: "MIJN BESTELLINGEN",
		menu_shopping_cart: "WINKELWAGEN",
		menu_news_promo: "NIEUWS & PROMO",
		menu_blog: "BLOG",
		menu_login: "INLOGGEN?",
		menu_logout: "UITLOGGEN",

		//HOME page
		lbl_day_offers: "DAGAANBIEDINGEN",
		btn_to_order: "BESTELLEN",
		lbl_lunch: "LUNCH",
		lbl_drinks: "DRANKEN",

		//Login Page
		lbl_login: "INLOGGEN",
		placeholder_uname: "Gebruikersnaam",
		placeholder_pwd: "Wachtwoord",
		btn_remember_me: "Remember me?",
		link_forgot_pwd: "Paswoord vergeten?",
		btn_login: "Log in",
		btn_register: "Register",

		//Register Page
		lbl_welcome: "Welkom",
		lbl_create_an_acc: "Account aanmaken",
		err_fname_req: "Voornaam is verplicht.",
		err_lname_req: "Achternaam is verplicht.",
		err_email_req:"Voer geldige e-mail-ID in.",
		lbl_new_user: "Nieuwe gebruiker?",
		placeholder_fname: "Voornaam",
		placeholder_lname: "Achternaam",
		placeholder_email: "E-mail",
		btn_signup: "Aanmelden",
		lbl_agreement_text: "Door een account aan te maken, ga je akkoord met Ox, de",
		lbl_agreement_text1: "en privacy",

		//News & promo
		lbl_news:"NEWS",

		//Shopping Cart
		lbl_my_shoppimh_cart: "Mijn Winkelkar",
		lbl_products: "Product (en)",
		lbl_price: "PRIJS",
		lbl_unit_price: "UNIT PRICE:",
		lbl_total_price: "TOTAL PRICE:",
		lbl_discount: "Korting",
		lbl_total: "Totaal",
		lbl_you_will_earn: "Je zal verdienen",
		btn_complete_order: "Maak bestelling af",
		lbl_loyality_pts: "Loyalty punten(+)",
		lbl_loyality_pts_use: "Te gebruiken loyalty punten(-)",
		lbl_discount_code: "Kortingscode",
		btn_to_apply: "Toepassen",
		lbl_use_my_reward_pts: "Gebruik mijn beloningspunten, ",
		lbl_availble: " beschikbaar ",
		lbl_coupn_applied: "De couponcode is toegepast",
		lbl_enter_cc: "Ingevoerde kortingscode",


		lbl_readmore: "LEES VERDER",
		lbl_ordersummary: "overzicht van de bestelling",
		lbl_product2: "PRODUCTEN",
		lbl_qty: "QTY",
		lbl_total: "TOTAAL",
		lbl_desc: "BESCHRIJVING",
		lbl_total_disc: "Totale korting",
		lbl_youremail: "jouw e-mailadres",
		lbl_reorder: "Opnieuw bestellen",

		//blog and blog detail
		lbl_blogtitle: "BLOG",
		
		//Change password
		lbl_chnageheading: "MIJN ACCOUNT - wachtwoord wijzigen",
		lbl_Olpass: "Oud Wachtwoord:",
		lbl_newpass: "Nieuw paswoord:",
		lbl_confirmnewpass: "Bevestig wachtwoord:",

		lbl_Olpassvalidate: "Oud wachtwoord is verplicht",
		lbl_newpassvalidate: "Nieuw wachtwoord is vereist",
		lbl_confirmnewpassvalidate: "Bevestig wachtwoord is verplicht",
		lbl_passbutton: "Paswoord wijzigen",

		//Confirm Order
		lbl_confirmorder: "Bevestig je bestelling",
		lbl_confirmbutton: "Bevestig bestelling",
		lbl_payment: "Betaling",
		lbl_paymentmethod: "Betalingsmiddel:",
		lbl_worldline: "Worldline",
		lbl_rewardpoints: "Beloningspunten",
		lbl_youwill: "Je zal verdienen",
		lbl_points: "points",
		lbl_loyaltybalance: "Loyalty Punten Saldo:",


		//email
		lbl_friendemail: "Email een vriend",
		lbl_friend: "Vrienden email",
		lbl_required: "Verplicht!",
		lbl_notvalidemail: "Geen geldige e-mail!",
		lbl_message: "Persoonlijk bericht",
		lbl_sendbutton: "Stuur een e-mail naar een vriend",
		lbl_youremail: "Your email address",

		//my orders	 and orderhistory
		lbl_myorder: "MIJN BESTELLINGEN",
		lbl_ordernumber: "Bestellingsnummer:",
		lbl_orderdate: "Besteldatum:",
		lbl_orderstatus: "Bestelstatus:",
		lbl_paymentstatus: "Betalingsstatus:",
		lbl_ordertotal: "Totale bestelling:",
		lbl_vieworder: "Bekijk bestelgegevens",
		lbl_noOrder: "Er zijn nog geen bestellingen geplaatst",
		lbl_orderhistory: "Bestelgeschiedenis",
		lbl_order: "Bestellen#",
		lbl_retry: "opnieuw proberen",

		//productdetail
		lbl_prodsdetail: "PRODUCT DETAIL",
		lbl_maylike: "Misschien vind je dit ook leuk",

		//product review
		lbl_prodreviews: "Product beoordelingen",
		lbl_writereview: "Schrijf uw eigen beoordeling",
		lbl_reviewtitle: "Titel bekijken",
		lbl_rvwtitlereq: "Beoordeling Titel is vereist.",
		lbl_rvwtext: "Beoordeling Tekst:",
		lbl_rating: "Beoordeling:",
		lbl_submitrvw: "Review versturen",
		lbl_exitrvw: "Bestaande beoordelingen",
		lbl_bad: "Slecht",
		lbl_exc: "Uitstekend",
		lbl_from: "Van:",
		lbl_date: "Datum:",

		//profile
		lbl_profile: "PROFIEL",
		lbl_email: "E-mail:",
		lbl_name: "Naam:",
		lbl_personal: "Persoonlijk nummer:",
		lbl_company: "Bedrijf:",

		//shipbill
		lbl_shipto: "Verzend naar:",
		lbl_selectAdd: "Select address",
		lbl_add1_required: "Adres 1 is verplicht.",
		lbl_email_required: "E-mail is verplicht.",
		lbl_city_required: "Plaats is verplicht.",
		lbl_zip_required: "Zip is verplicht.",
		lbl_phone_required: "Telefoon is verplicht.",
		btn_go_confirm: "Ga naar bestelling bevestigen",
		btn_save_and_update_add: "Bewaar en update adres"



	});


	$translateProvider.preferredLanguage("be");
	$translateProvider.fallbackLanguage("be");


	// Ionic uses AngularUI Router which uses the concept of states
	// Learn more here: https://github.com/angular-ui/ui-router
	// Set up the various states which the app can be in.
	// Each state's controller can be found in controllers.js
	$stateProvider
	  .state('menu.home', {
	  	url: '/page1',
	  	views: {
	  		'side-menu21': {
	  			templateUrl: 'templates/home.html',
	  			controller: 'homeCtrl'
	  		}
	  	}
	  })

	.state('menu.product', {
		url: '/page2',
		views: {
			'side-menu21': {
				templateUrl: 'templates/product.html',
				controller: 'productCtrl'
			}
		}
	})

	.state('menu.productDetail', {
		url: '/page3',
		views: {
			'side-menu21': {
				templateUrl: 'templates/productDetail.html',
				controller: 'productDetailCtrl'
			}
		}
	})

	.state('menu', {
		url: '/side-menu21',
		templateUrl: 'templates/menu.html',
		controller: 'menuCtrl'
	})

	.state('menu.login', {
		url: '/page4',
		views: {
			'side-menu21': {
				templateUrl: 'templates/login.html',
				controller: 'loginCtrl'
			}
		}
	})

	  .state('menu.signup', {
	  	url: '/page5',
	  	views: {
	  		'side-menu21': {
	  			templateUrl: 'templates/signup.html',
	  			controller: 'signupCtrl'
	  		}
	  	}
	  })

	.state('menu.profile', {
		url: '/page6',
		views: {
			'side-menu21': {
				templateUrl: 'templates/profile.html',
				controller: 'profileCtrl'
			}
		}
	})

	.state('menu.cart', {
		url: '/page7',
		views: {
			'side-menu21': {
				templateUrl: 'templates/cart.html',
				controller: 'cartCtrl',
				cache:false
			}
		}
	})


	.state('menu.news', {
		url: '/page8',
		views: {
			'side-menu21': {
				templateUrl: 'templates/news.html',
				controller: 'newsCtrl'
			}
		}
	})

	  .state('menu.newsdetail', {
	  	url: '/page27',
	  	views: {
	  		'side-menu21': {
	  			templateUrl: 'templates/newsdetail.html',
	  			controller: 'newsDetailsCtrl'
	  		}
	  	}
	  })


	.state('menu.promo', {
		url: '/page9',
		views: {
			'side-menu21': {
				templateUrl: 'templates/promo.html',
				controller: 'promoCtrl'
			}
		}
	})

	.state('menu.blog', {
		url: '/page10',
		views: {
			'side-menu21': {
				templateUrl: 'templates/blog.html',
				controller: 'blogCtrl'
			}
		}
	})

	  .state('menu.blogDetail', {
	  	url: '/page28',
	  	views: {
	  		'side-menu21': {
	  			templateUrl: 'templates/blogdetail.html',
	  			controller: 'blogDetailsCtrl'	  			
	  		}
	  	}
	  })


	.state('menu.payment', {
		url: '/page11',
		views: {
			'side-menu21': {
				templateUrl: 'templates/payment.html',
				controller: 'paymentCtrl'				
			}
		}
	})

	.state('menu.order', {
		url: '/page12',
		views: {
			'side-menu21': {
				templateUrl: 'templates/order.html',
				controller: 'orderCtrl'
			}
		}
	})


	  .state('menu.myOrders', {
	  	url: '/page19',
	  	views: {
	  		'side-menu21': {
	  			templateUrl: 'templates/myOrders.html',
	  			controller: 'myOrdersCtrl'
	  		}
	  	}
	  })


	 .state('menu.orderhistory', {
	 	url: '/page17',
	 	views: {
	 		'side-menu21': {
	 			templateUrl: 'templates/orderHistory.html',
	 			controller: 'orderHistoryCtrl'
	 		}
	 	}
	 })


	  //.state('menu.Confirmorder', {
	  //	url: '/page15',
	  //	views: {
	  //		'side-menu21': {
	  //			templateUrl: 'templates/Order.html',
	  //			controller: 'placeOrderCtrl'
	  //		}
	  //	}
	  //})


	 .state('menu.placeorder', {
	 	url: '/page16',
	 	views: {
	 		'side-menu21': {
	 			templateUrl: 'templates/order.html',
	 			controller: 'confirmOrderCtrl'
	 		}
	 	}
	 })



	.state('menu.thanks', {
		url: '/page13',
		views: {
			'side-menu21': {
				templateUrl: 'templates/thanks.html',
				controller: 'thanksCtrl'
			}
		}
	})

	  .state('menu.newslist', {
	  	url: '/page14',
	  	views: {
	  		'side-menu21': {
	  			templateUrl: 'templates/newslist.html',
	  			controller: 'newsListCtrl'
	  		}
	  	}
	  })

	  .state('menu.bloglist', {
	  	url: '/page15',
	  	views: {
	  		'side-menu21': {
	  			templateUrl: 'templates/bloglist.html',
	  			controller: 'blogListCtrl'
	  		}
	  	}
	  })

	  .state('menu.editProfile', {
	  	url: '/page20',
	  	views: {
	  		'side-menu21': {
	  			templateUrl: 'templates/editProfile.html',
	  			controller: 'editProfileCtrl'
	  		}
	  	}
	  })

	 .state('menu.Emailtofriend', {
	 	url: '/page29',
	 	views: {
	 		'side-menu21': {
	 			templateUrl: 'templates/EmailFriend.html',
	 			controller: 'emailFriendCtrl'
	 		}
	 	}
	 })


.state('menu.ProductReviews', {
	url: '/page31',
	views: {
		'side-menu21': {
			templateUrl: 'templates/ProductReviews.html',
			controller: 'productReviewsCtrl'
		}
	}
})

	.state('menu.ConfirmOrder', {
		url: '/page32',
		views: {
			'side-menu21': {
				templateUrl: 'templates/confirmOrder.html',
				controller: 'confirmOrderCtrl'
			}
		}
	})

	.state('menu.Shipbill', {
		url: '/page33',
		views: {
			'side-menu21': {
				templateUrl: 'templates/shipbill.html',
				controller: 'shipbillCtrl'
			}
		}
	})

		.state('menu.changePassword', {
			url: '/page34',
			views: {
				'side-menu21': {
					templateUrl: 'templates/changePassword.html',
					controller: 'changePWDCtrl'
				}
			}
		})

		.state('menu.disclaimer', {
			url: '/page36',
			views: {
				'side-menu21': {
					templateUrl: 'templates/Disclaimer.html',
					controller: 'disclaimerCtrl'
				}
			}
		})


		.state('menu.Aboutus', {
			url: '/page35',
			views: {
				'side-menu21': {
					templateUrl: 'templates/AboutUs.html',
					controller: 'aboutCtrl'
				}
			}
		})


	$urlRouterProvider.otherwise('/side-menu21/page1')



});