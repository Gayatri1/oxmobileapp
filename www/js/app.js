// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)

// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.directives', 'app.services', 'pascalprecht.translate'])

.config(function ($ionicConfigProvider, $sceDelegateProvider, $translateProvider)
{

	$sceDelegateProvider.resourceUrlWhitelist(['self', '*://www.youtube.com/**', '*://player.vimeo.com/video/**']);
	//$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|ftp|blob):|data:image\//);

})

.run(function ($ionicPlatform, $translate)
{
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }


  	///https://www.thepolyglotdeveloper.com/2014/08/internationalization-localization-ionicframework-angular-translate/
    //if (typeof navigator.globalization !== "undefined")
    //{
    //	navigator.globalization.getPreferredLanguage(function (language)
    //	{
    //		$translate.use((language.value).split("-")[0]).then(function (data)
    //		{
    //			console.log("SUCCESS -> " + data);
    //		}, function (error)
    //		{
    //			console.log("ERROR -> " + error);
    //		});
    //	}, null);
    //}



    $ionicPlatform.onHardwareBackButton(function ()
    {
    	event.preventDefault();
    	function showConfirm()
    	{
    		var confirmPopup = $ionicPopup.show({
    			title: 'Exit Ox App?',
    			template: 'Are you sure you want to exit Ox App?',
    			buttons: [{
    				text: 'Cancel',
    				type: 'button-royal button-outline',
    			}, {
    				text: 'Ok',
    				type: 'button-royal',
    				onTap: function ()
    				{
    					ionic.Platform.exitApp();
    				}
    			}]
    		});
    	};

    	// Is there a page to go back to?
    	if ($ionicHistory.backView())
    	{
    		// Go back in history
    		$ionicHistory.backView().go();
    	} else
    	{
    		// This is the last page: Show confirmation popup
    		showConfirm();
    	}

    	return false;
    }, 101);

  });
  
   //$ionicPlatform.onHardwareBackButton(function () {
   	
   //	event.preventDefault();
   //	event.stopPropagation();	     
   //}, 100);






})

/*
  This directive is used to disable the "drag to open" functionality of the Side-Menu
  when you are dragging a Slider component.
*/
.directive('disableSideMenuDrag', ['$ionicSideMenuDelegate', '$rootScope', function($ionicSideMenuDelegate, $rootScope) {
    return {
        restrict: "A",  
        controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {

            function stopDrag(){
              $ionicSideMenuDelegate.canDragContent(false);
            }

            function allowDrag(){
              $ionicSideMenuDelegate.canDragContent(true);
            }

            $rootScope.$on('$ionicSlides.slideChangeEnd', allowDrag);
            $element.on('touchstart', stopDrag);
            $element.on('touchend', allowDrag);
            $element.on('mousedown', stopDrag);
            $element.on('mouseup', allowDrag);

        }]
    };
}])

/*
  This directive is used to open regular and dynamic href links inside of inappbrowser.
*/
.directive('hrefInappbrowser', function() {
  return {
    restrict: 'A',
    replace: false,
    transclude: false,
    link: function(scope, element, attrs) {
      var href = attrs['hrefInappbrowser'];

      attrs.$observe('hrefInappbrowser', function(val){
        href = val;
      });
      
      element.bind('click', function (event) {

        window.open(href, '_system', 'location=yes');

        event.preventDefault();
        event.stopPropagation();

      });
    }
  };
});